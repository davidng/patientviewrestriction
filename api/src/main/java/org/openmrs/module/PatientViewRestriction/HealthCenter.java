package org.openmrs.module.PatientViewRestriction;

import java.util.Date;

import org.openmrs.Auditable;
import org.openmrs.BaseOpenmrsObject;
import org.openmrs.User;

public class HealthCenter extends BaseOpenmrsObject implements Auditable {

	private Integer id;
	private Integer locationId;
	private String userIds;
	private String roleIds;

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer arg0) {
		id = arg0;
	}
	
	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getUserIds() {
		return userIds;
	}

	public void setUserIds(String userIds) {
		this.userIds = userIds;
	}

	public String getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(String roleIds) {
		this.roleIds = roleIds;
	}


	@Override
	public User getChangedBy() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getCreator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getDateChanged() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getDateCreated() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setChangedBy(User arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCreator(User arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDateChanged(Date arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDateCreated(Date arg0) {
		// TODO Auto-generated method stub
		
	}
}