/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.openmrs.module.PatientViewRestriction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Role;


public class RoleListItem {
	
	protected final Log log = LogFactory.getLog(getClass());
	
	private String roleId;
	
	private String name;
	
	public RoleListItem() {
	}
	
	public RoleListItem(Role role) {
		
		if (role!= null) {
			roleId = role.getName();
			name = role.getName();
		}
	}
	
	public String getRoleId() {
		return roleId;
	}
	
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
}
