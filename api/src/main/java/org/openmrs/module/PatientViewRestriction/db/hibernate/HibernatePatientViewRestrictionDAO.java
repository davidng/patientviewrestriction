package org.openmrs.module.PatientViewRestriction.db.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.openmrs.Role;
import org.openmrs.api.db.DAOException;
import org.openmrs.module.PatientViewRestriction.db.PatientViewRestrictionDAO;
import org.openmrs.module.PatientViewRestriction.HealthCenter;
import org.openmrs.module.PatientViewRestriction.LocationItem;

/**
 * Database interface for the module
 * 
 * @author Citigo
 * 
 */
public class HibernatePatientViewRestrictionDAO implements PatientViewRestrictionDAO {

	/**
	 * Hibernate session factory
	 */
        
	private SessionFactory sessionFactory;

	/**
	 * Default public constructor
	 */
	public HibernatePatientViewRestrictionDAO() {
	}

	/**
	 * Set session factory
	 * 
	 * @param sessionFactory
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<LocationItem> getAllHealthCenters() {
		List<LocationItem> result = new ArrayList<LocationItem>();
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				LocationItem.class);
		result = (List<LocationItem>) criteria.list();
		return result;
	}
	
	public HealthCenter getHealthCenter(int locationId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(HealthCenter.class);
        criteria.add(Expression.eq("locationId", locationId));
        return (HealthCenter)criteria.uniqueResult();
	}

	public void storeHealthCenter(HealthCenter center) {
		sessionFactory.getCurrentSession().saveOrUpdate(center);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Role> getRoles(String nameFragment, Integer start, Integer length)
	        throws DAOException {
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Role.class);
		
		if (StringUtils.isNotBlank(nameFragment))
			criteria.add(Restrictions.ilike("role", nameFragment, MatchMode.START));
		
		criteria.addOrder(Order.asc("role"));
		if (start != null)
			criteria.setFirstResult(start);
		if (length != null && length > 0)
			criteria.setMaxResults(length);
		
		return criteria.list();
	}      
	    
	public Role getRole(String role) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Role.class);
		criteria.add(Expression.eq("role", role));
		return (Role) criteria.uniqueResult();
	}
	    
	public List<Role> getAllRoles(){
	        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Role.class);		
	        criteria.addOrder(Order.asc("role"));
	        return criteria.list();
	}
}