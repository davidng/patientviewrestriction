package org.openmrs.module.PatientViewRestriction.db;

import java.util.List;

import org.openmrs.Role;
import org.openmrs.api.db.DAOException;
import org.openmrs.module.PatientViewRestriction.HealthCenter;
import org.openmrs.module.PatientViewRestriction.LocationItem;

/**
 * Public Interface to the {@link}HibernatePatientViewRestrictionDAO
 * 
 * @author Citigo
 */
public interface PatientViewRestrictionDAO {
	public List<LocationItem> getAllHealthCenters();
	
	public HealthCenter getHealthCenter(int locationId);
	
	public void storeHealthCenter(HealthCenter center);
        
	public List<Role> getRoles(String nameFragment, Integer start, Integer length) throws DAOException;
    
    public Role getRole(String role) throws DAOException;
    
    public List<Role> getAllRoles() throws DAOException;
}