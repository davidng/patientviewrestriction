package org.openmrs.module.PatientViewRestriction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.module.BaseModuleActivator;

/**
 * Activator startup/shutdown methods for the PatientViewRestriction module
 *
 * @author  Citigo
 */
public class PatientViewRestrictionActivator extends BaseModuleActivator {

	private Log log = LogFactory.getLog(this.getClass());
	
	@Override
	public void willStart() {
		super.willStart();
		log.info("Starting the Patient View Restriction module");
	}
	
	@Override
	public void willStop() {
		super.willStop();
		log.info("Shutting down the Patient View Restriction module");
	}	
}
