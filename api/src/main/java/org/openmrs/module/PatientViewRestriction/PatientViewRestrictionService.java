package org.openmrs.module.PatientViewRestriction;

/**
 * Required methods for Patient View Restriction service
 * 
 * @author  Citigo
 */

import java.util.List;

import org.openmrs.Role;
import org.openmrs.annotation.Authorized;
import org.openmrs.api.APIException;
import org.openmrs.util.PrivilegeConstants;
import org.springframework.transaction.annotation.Transactional;

public interface PatientViewRestrictionService {
	public void setHealthCenterType(int type);
	public int getHealthCenterType();
	public List<LocationItem> getAllHealthCenters();
	public HealthCenter getHealthCenter(int locationId);
	public void storeHealthCenter(HealthCenter center);
	
    @Transactional(readOnly = true)
	@Authorized( { PrivilegeConstants.VIEW_ROLES })
	public List<Role> getRoles(String nameFragment, Integer start, Integer length)
        throws APIException;
    
    @Transactional(readOnly = true)
	@Authorized( { PrivilegeConstants.VIEW_ROLES })
	public Role getRole(String role) throws APIException;  

    @Transactional(readOnly = true)
	@Authorized( { PrivilegeConstants.VIEW_ROLES })
	public List<Role> getAllRoles() throws APIException;
}