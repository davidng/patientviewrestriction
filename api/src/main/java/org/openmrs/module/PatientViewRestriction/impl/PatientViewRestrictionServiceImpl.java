package org.openmrs.module.PatientViewRestriction.impl;

import java.util.List;

import org.openmrs.Role;
import org.openmrs.api.APIException;
import org.openmrs.api.AdministrationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.PatientViewRestriction.HealthCenter;
import org.openmrs.module.PatientViewRestriction.LocationItem;
import org.openmrs.module.PatientViewRestriction.PatientViewRestrictionService;
import org.openmrs.module.PatientViewRestriction.db.PatientViewRestrictionDAO;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Samuel Mbugua
 */
@Transactional
@Component
public class PatientViewRestrictionServiceImpl implements PatientViewRestrictionService {
	      
    private PatientViewRestrictionDAO patientViewRestrictionDAO;
	
	public PatientViewRestrictionServiceImpl() {
		 
	}

    public PatientViewRestrictionDAO getPatientViewRestrictionDAO() {
        return patientViewRestrictionDAO;
    }

    public void setPatientViewRestrictionDAO(PatientViewRestrictionDAO patientViewRestrictionDAO) {
        this.patientViewRestrictionDAO = patientViewRestrictionDAO;
    }
    
    public void setHealthCenterType(int type){
    	AdministrationService adminService = Context.getAdministrationService();
    	adminService.setGlobalProperty("patientviewrestriction.healthcenter.type", String.valueOf(type));
    }
    
    public int getHealthCenterType(){
    	AdministrationService adminService = Context.getAdministrationService();
    	try{
    		return Integer.valueOf(adminService.getGlobalProperty("patientviewrestriction.healthcenter.type"));
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return -1;
    }
    
    public List<LocationItem> getAllHealthCenters(){
    	return patientViewRestrictionDAO.getAllHealthCenters();
    }
    
	public HealthCenter getHealthCenter(int locationId) {
		return patientViewRestrictionDAO.getHealthCenter(locationId);
	}

	public void storeHealthCenter(HealthCenter center) {
		patientViewRestrictionDAO.storeHealthCenter(center);
	}
    
    public List<Role> getRoles(String nameFragment, Integer start, Integer length)throws APIException{
        return patientViewRestrictionDAO.getRoles(nameFragment, start, length);
	}
	
	public Role getRole(String role) throws APIException{
	        return patientViewRestrictionDAO.getRole(role);
	}
	
	public List<Role> getAllRoles() throws APIException{
	        return patientViewRestrictionDAO.getAllRoles();
	}
}