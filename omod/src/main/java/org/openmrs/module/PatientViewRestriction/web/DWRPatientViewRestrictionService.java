/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openmrs.module.PatientViewRestriction.web;


import java.util.List;
import java.util.Vector;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.openmrs.Role;
import org.openmrs.api.APIException;

import org.openmrs.api.context.Context;
import org.openmrs.messagesource.MessageSourceService;
import org.openmrs.module.PatientViewRestriction.PatientViewRestrictionService;
import org.openmrs.module.PatientViewRestriction.RoleListItem;

/**
 *
 * @author guess
 */
public class DWRPatientViewRestrictionService {
    
    private static final Log log = LogFactory.getLog(DWRPatientViewRestrictionService.class);
    
    public Vector<Object> findBatchOfRoles(String searchValue, Integer start, Integer length)
	        throws APIException {
		
		Vector<Object> roleList = new Vector<Object>();
		MessageSourceService mss = Context.getMessageSourceService();
		
		try {                        
            PatientViewRestrictionService ps = Context.getService(PatientViewRestrictionService.class);
			List<Role> roles = ps.getRoles(searchValue, start, length);
			roleList = new Vector<Object>(roles.size());
			
			for (Role role : roles) {
				roleList.add(new RoleListItem(role));
			}
		}
		catch (Exception e) {
			log.error(e);
			//roleList.add(mss.getMessage("Role.search.error") + " - " + e.getMessage());
		}
		
		if (roleList.size() == 0) {
			//roleList.add(mss.getMessage("Role.noLocationsFound"));
		}
		
		return roleList;
	}
    
    public RoleListItem getRole(String role) {
    	PatientViewRestrictionService ps = Context.getService(PatientViewRestrictionService.class);
		Role r = ps.getRole(role);		
		return r == null ? null : new RoleListItem(r);
	}
    
    public Vector findRoles(String searchValue) {
		
		return findBatchOfRoles(searchValue, null, null);
	}
 
    public Vector getAllRoles(){
        Vector roleList = new Vector();
		MessageSourceService mss = Context.getMessageSourceService();
		
		try {                        
			PatientViewRestrictionService ps = Context.getService(PatientViewRestrictionService.class);
			List<Role> roles = ps.getAllRoles();
                        roleList = new Vector(roles.size());
			for (Role role : roles) {                                
                                roleList.add(new RoleListItem(role));                                
			}
		}
		catch (Exception e) {
			log.error(e);
			//roleList.add(mss.getMessage("Role.search.error") + " - " + e.getMessage());
		}
		
		if (roleList.size() == 0) {
			//roleList.add(mss.getMessage("Role.noLocationsFound"));
		}
		
		return roleList;
    }
}
