package org.openmrs.module.PatientViewRestriction.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Role;
import org.openmrs.User;
import org.openmrs.api.UserService;
import org.openmrs.api.context.Context;
import org.openmrs.module.PatientViewRestriction.HealthCenter;
import org.openmrs.module.PatientViewRestriction.LocationItem;
import org.openmrs.module.PatientViewRestriction.PatientViewRestrictionService;
import org.openmrs.web.WebConstants;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for all Patient View Restriction pages
 * 
 * @author Citigo
 */
@Controller
public class PatientViewRestrictionController {
	
	//private static AdministrationService adminService = Context.getAdministrationService();
	
	/** Logger for this class and subclasses */
	protected final Log log = LogFactory.getLog(getClass());
	
	//a single instance of module service
	private PatientViewRestrictionService pvs;
	
	@RequestMapping(value="/module/PatientViewRestriction/healthcenters", method=RequestMethod.GET)
	public String getHealthCenters(ModelMap model, HttpServletRequest request) {
		getPatientViewRestrictionService();
		List<LocationItem> listHealthCenter = pvs.getAllHealthCenters();
		model.addAttribute("healthCenters", listHealthCenter);
		
		return "/module/PatientViewRestriction/healthcenters";
	}
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value="/module/PatientViewRestriction/settings", method=RequestMethod.GET)
	public String getSettings(ModelMap model, HttpServletRequest request) {
		getPatientViewRestrictionService();

		Integer healthCenterType = -1;
		try{
			healthCenterType = pvs.getHealthCenterType();	
		}catch(Exception e){
			e.printStackTrace();
		}
		
		Integer selectedHealthCenterType = -1;
		try{
			selectedHealthCenterType = Integer.parseInt(request.getParameter("selectedHealthCenterType"));			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(selectedHealthCenterType > 0){
			pvs.setHealthCenterType(selectedHealthCenterType);
			request.getSession().setAttribute(WebConstants.OPENMRS_MSG_ATTR, "Health Center Type saved.");
		}else{
			selectedHealthCenterType = healthCenterType;
		}

		model.addAttribute("selectedHealthCenterType", selectedHealthCenterType);
		return "/module/PatientViewRestriction/settings";
	}
	
	@RequestMapping(value="/module/PatientViewRestriction/user_role", method=RequestMethod.GET)
	public String getUsersOrRoles(ModelMap model, HttpServletRequest request) {
		UserService us = Context.getUserService();
		
		getPatientViewRestrictionService();
		Integer locationId = 0;
		try{
			locationId = Integer.valueOf(request.getParameter("location_id"));
		}catch(Exception e){}
		
		List<User> listUser = new ArrayList<User>();
		List<Role> listRole = new ArrayList<Role>();
		
		if(locationId > 0){
			HealthCenter center = pvs.getHealthCenter(locationId);
			if(center != null){
				String userIds = center.getUserIds();
				if(userIds == null) userIds = "";
				String roleIds = center.getRoleIds();
				if(roleIds == null) roleIds = "";
				
				String[] listUserId = userIds.split(",");
				for(String userId : listUserId){
					userId = userId.trim();
					if(userId.isEmpty()) continue;
					try{
						User user = us.getUser(Integer.valueOf(userId));
						listUser.add(user);
					}catch(Exception e){}
				}
				
				String[] listRoleName = roleIds.split(",");
				for(String roleName : listRoleName){
					roleName = roleName.trim();
					if(roleName.isEmpty()) continue;
					Role role = pvs.getRole(roleName);
					if(role != null)
						listRole.add(role);
				}
			}
		}
		
		model.addAttribute("users", listUser);
		model.addAttribute("roles", listRole);
		model.addAttribute("locationId", locationId);
		return "/module/PatientViewRestriction/user_role";
	}
	
	@RequestMapping(value="/module/PatientViewRestriction/user_role", method=RequestMethod.POST)
	public String saveUsersOrRoles(ModelMap model, HttpServletRequest request, @RequestParam String userIds, @RequestParam String roleIds, @RequestParam Integer locationId) {
		getPatientViewRestrictionService();
		
		HealthCenter center = pvs.getHealthCenter(locationId);
		if(center == null){
			center = new HealthCenter();
			center.setLocationId(locationId);
		}
		
		center.setUserIds(userIds);
		center.setRoleIds(roleIds);
		pvs.storeHealthCenter(center);
		
		return "redirect:healthcenters.list";
	}
		
	private void getPatientViewRestrictionService(){
		if (pvs == null)
			pvs=(PatientViewRestrictionService)Context.getService(PatientViewRestrictionService.class);
	}
}
