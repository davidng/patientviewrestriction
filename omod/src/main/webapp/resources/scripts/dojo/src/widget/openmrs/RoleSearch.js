/*
	Copyright (c) 2006, The OpenMRS Cooperative
	All Rights Reserved.
*/

dojo.provide("dojo.widget.openmrs.RoleSearch");
dojo.require("dojo.widget.openmrs.OpenmrsSearch");

var openmrsSearchBase = djConfig["baseScriptUri"].substring(0, djConfig["baseScriptUri"].indexOf("/", 1));
//importJavascriptFile(openmrsSearchBase + "/dwr/interface/DWREncounterService.js");
importJavascriptFile(openmrsSearchBase + "/dwr/interface/DWRPatientViewRestrictionService.js");

dojo.widget.tags.addParseTreeHandler("dojo:RoleSearch");

dojo.widget.defineWidget(
	"dojo.widget.openmrs.RoleSearch",
	dojo.widget.openmrs.OpenmrsSearch,
	{
		locationId: "",
		
		postCreate: function(){
			dojo.debug("postCreate in RoleSearch");
			if (this.locationId)
				DWRPatientViewRestrictionService.getRole(this.locationId, this.simpleClosure(this, "select"));
				//DWREncounterService.getLocation(this.locationId, this.simpleClosure(this, "select"));
		},
		
		showAll: function() {
			DWRPatientViewRestrictionService.getAllRoles(this.simpleClosure(this, "doObjectsFound"));
			//DWREncounterService.getLocations(this.simpleClosure(this, "doObjectsFound"));
		},
		
		doFindObjects: function(text) {
			DWRPatientViewRestrictionService.findRoles(text, this.simpleClosure(this, "doObjectsFound"));
			//DWREncounterService.findLocations(text, this.simpleClosure(this, "doObjectsFound"));
			return false;
		},
	
		getName: function(loc) {
			if (typeof loc == 'string') return this.noCell();
			return loc.name;
		},
		
		getCellFunctions: function() {
			return [this.simpleClosure(this, "getNumber"), 
					this.simpleClosure(this, "getName")
					];
			
		},
			
		// TODO: internationalize
		showHeaderRow: true,
		getHeaderCellContent: function() {
			return ['', 'Role Name'];
		},
		
		searchCleared: function() {
			this.showAll();
		}
	},
	"html"
);