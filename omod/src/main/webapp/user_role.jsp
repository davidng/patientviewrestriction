<%@ include file="/WEB-INF/template/include.jsp" %>

<openmrs:require privilege="Manage Patient View Restriction" otherwise="/login.htm" redirect="/module/PatientViewRestriction/healthcenters.form"/>

<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp"%>
<openmrs:htmlInclude file="/moduleResources/PatientViewRestriction/scripts/dojo/dojo.js"/>

<script type="text/javascript">
	dojo.require("dojo.widget.openmrs.UserSearch");
	dojo.require("dojo.widget.openmrs.RoleSearch");
	dojo.require("dojo.widget.openmrs.OpenmrsPopup");
	
	dojo.addOnLoad( function() {
		
		searchWidget = dojo.widget.manager.getWidgetById("uSearch");		
		
		dojo.event.topic.subscribe("uSearch/select", 
			function(msg) {
				for (var i=0; i< msg.objs.length; i++) {
					var obj = msg.objs[i];
					var options = $("userNames").options;
					var isAddable = true;
					for (x=0; x<options.length; x++)
						if (options[x].value == obj.userId)
							isAddable = false;
					
					if (isAddable) {
						var opt = new Option(obj.personName, obj.userId);
						opt.selected = true;
						options[options.length] = opt;
						copyIds("userNames", "userIds", ",");
					}
				}
			}
		);

		dojo.event.topic.subscribe("rSearch/select", 
			function(msg) {
				for (var i=0; i< msg.objs.length; i++) {
					var obj = msg.objs[i];
					var options = $("roleNames").options;
					var isAddable = true;
					for (x=0; x<options.length; x++)
						if (options[x].value == obj.roleId)
							isAddable = false;
					
					if (isAddable) {
						var opt = new Option(obj.name, obj.roleId);
						opt.selected = true;
						options[options.length] = opt;
						copyIds("roleNames", "roleIds", ",");
					}
				}	
			}
		);
	});
	
</script>

<script type="text/javascript">
		
	function removeItem(nameList, idList, delim)
	{
		var sel   = document.getElementById(nameList);
		var input = document.getElementById(idList);
		var optList   = sel.options;
		var lastIndex = -1;
		var i = 0;
		while (i<optList.length) {
			// loop over and erase all selected items
			if (optList[i].selected) {
				optList[i] = null;
				lastIndex = i;
			}
			else {
				i++;
			}
		}
		copyIds(nameList, idList, delim);
		while (lastIndex >= optList.length)
			lastIndex = lastIndex - 1;
		if (lastIndex >= 0) {
			optList[lastIndex].selected = true;
			return optList[lastIndex];
		}
		return null;
	}
	
	function copyIds(from, to, delimiter)
	{
		var sel = document.getElementById(from);
		var input = document.getElementById(to);
		var optList = sel.options;
		var remaining = new Array();
		var i=0;
		while (i < optList.length)
		{
			remaining.push(optList[i].value);
			i++;
		}
		input.value = remaining.join(delimiter);
	}
	
	function listKeyPress(from, to, delim, event) {
		var keyCode = event.keyCode;
		if (keyCode == 8 || keyCode == 46) {
			removeItem(from, to, delim);
			//attempt to prevent backspace key (#8) from going back in browser
			window.Event.keyCode = 0;
		}
	}
</script>


<div>
	<b class="boxHeader">
		Assign users/roles to health center
	</b>
	<form method="post" onSubmit="return true;">
		<div class="box">
			<input type="hidden" name="locationId" value="${locationId}"/>
			<table>
				<tr>
					<td valign="top"><spring:message code="PatientViewRestriction.users"/></td>
					<td valign="top">
						<input type="hidden" name="userIds" id="userIds" size="40" value='<c:forEach items="${users}" var="user">${user.id},</c:forEach>' />
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top">
									<select class="selectWidth" size="6" id="userNames" multiple onkeyup="listKeyPress('userNames', 'userIds', ',', event);">
										<c:forEach items="${users}" var="user">
											<option value="${user.id}">${user.personName}</option>
										</c:forEach>
									</select>
								</td>
								<td valign="top" class="buttons">
									&nbsp;<span dojoType="UserSearch" widgetId="uSearch"></span><span dojoType="OpenmrsPopup" searchWidget="uSearch" searchTitle='<spring:message code="PatientViewRestriction.users.search"/>' changeButtonValue='<spring:message code="general.add"/>'></span>
									&nbsp; <input type="button" value="<spring:message code="general.remove"/>" class="smallButton" onClick="removeItem('userNames', 'userIds', ',');" /> <br/><br/>
								</td>
							</tr>
						</table>
					</td>
					<td style="width:50px;"></td>


					<td valign="top"><spring:message code="PatientViewRestriction.roles"/></td>
					<td valign="top">
						<input type="hidden" name="roleIds" id="roleIds" size="40" value='<c:forEach items="${roles}" var="role">${role.name},</c:forEach>' />
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top">
									<select class="selectWidth" size="6" id="roleNames" multiple onkeyup="listKeyPress('roleNames', 'roleIds', ',', event);">
										<c:forEach items="${roles}" var="role">
											<option value="${role.name}">${role.name}</option>
										</c:forEach>		
									</select>
								</td>
								<td valign="top" class="buttons">
									&nbsp;<span dojoType="RoleSearch" widgetId="rSearch"></span><span dojoType="OpenmrsPopup" searchWidget="rSearch" searchTitle='<spring:message code="PatientViewRestriction.roles.search"/>' changeButtonValue='<spring:message code="general.add"/>'></span>
									&nbsp; <input type="button" value="<spring:message code="general.remove"/>" class="smallButton" onClick="removeItem('roleNames', 'roleIds', ',');" /> <br/><br/>
									&nbsp;
								</td>
							</tr>
						</table>
					</td>

				</tr>
			</table>
			
		</div>
		<br/>
		<a href="healthcenters.list"><input type="button" value="<spring:message code="PatientViewRestriction.discardChanges"/>"/></a>
		<input type="submit" value="<spring:message code="PatientViewRestriction.save"/>"/>
	</form>
</div>
<br/>
<%@ include file="/WEB-INF/template/footer.jsp" %>