<ul id="menu">
	<li class="first">
		<a href="${pageContext.request.contextPath}/admin"><spring:message code="admin.title.short"/></a>
	</li>
	
	<openmrs:hasPrivilege privilege="Manage Patient View Restriction">
		<li <c:if test='<%= request.getRequestURI().contains("PatientViewRestriction/healthcenters") %>'>class="active"</c:if>>
			<a href="${pageContext.request.contextPath}/module/PatientViewRestriction/healthcenters.list">
				<spring:message code="PatientViewRestriction.manage.healthcenters"/>
			</a>
		</li>
	</openmrs:hasPrivilege>
	
	<openmrs:hasPrivilege privilege="Manage Patient View Restriction">
		<li <c:if test='<%= request.getRequestURI().contains("PatientViewRestriction/settings") %>'>class="active"</c:if>>
			<a href="${pageContext.request.contextPath}/module/PatientViewRestriction/settings.list">
				<spring:message code="PatientViewRestriction.settings"/>
			</a>
		</li>
	</openmrs:hasPrivilege>

</ul>