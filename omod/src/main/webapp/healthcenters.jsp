<%@ include file="/WEB-INF/template/include.jsp" %>

<openmrs:require privilege="Manage Patient View Restriction" otherwise="/login.htm" redirect="/module/PatientViewRestriction/healthcenters.list"/>

<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp"%>
<openmrs:htmlInclude file="/scripts/dojo/dojo.js" />
<openmrs:htmlInclude file="/scripts/dojoConfig.js" />

<c:set var="healthCentersSize" value="${fn:length(healthCenters)}" />

<div>
	<b class="boxHeader">
		<spring:message code="PatientViewRestriction.manage.healthcenters" />
	</b>
	<c:choose>
		<c:when test="${healthCentersSize < 1}">
			<br/>&nbsp;&nbsp;<i>(<spring:message code="PatientViewRestriction.null"/>)</i><br/>
		</c:when>
		<c:otherwise>
			<table>
				<tr>
					<th colspan="2"><u><spring:message code="PatientViewRestriction.healthcenter"/></u></th>
				</tr>
				<c:forEach items="${healthCenters}" var="loc" varStatus="varStatus">
					<tr class="<c:choose><c:when test="${varStatus.index % 2 == 0}">evenRow</c:when><c:otherwise>oddRow</c:otherwise></c:choose>">
						<td>${loc.name}</td>
						<td>
							<a href="${pageContext.request.contextPath}/module/PatientViewRestriction/user_role.form?location_id=${loc.id}">
								Assign Users/Roles
							</a>
						</td>
					</tr>
				</c:forEach>
			</table>
		</c:otherwise>
	</c:choose>
</div>
<br/>
<%@ include file="/WEB-INF/template/footer.jsp" %>