<%@ include file="/WEB-INF/template/include.jsp" %>

<openmrs:require privilege="Manage Patient View Restriction" otherwise="/login.htm" redirect="/module/PatientViewRestriction/settings.list"/>

<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp"%>
<openmrs:htmlInclude file="/scripts/dojo/dojo.js" />
<openmrs:htmlInclude file="/scripts/dojoConfig.js" />

<script type="text/javascript">
var selected = "${selectedHealthCenterType}";
function onChange(selection){
	selected = selection.options[selection.selectedIndex].value;
}
function onSave(){
	window.location = '${pageContext.request.contextPath}/module/PatientViewRestriction/settings.list?selectedHealthCenterType=' + selected;
}
</script>
<div>
	<b class="boxHeader">
		<spring:message code="PatientViewRestriction.settings" />
	</b>
</div>
<br/>
<div>
<spring:message code="PatientViewRestriction.select.healthcenter.type" /> :
<br/>
<select name="healthCenterType" onchange="javascript:onChange(this);">
	<option value="1" <c:if test="${selectedHealthCenterType == 1}">selected</c:if>><spring:message code="PatientViewRestriction.healthcenter.first.assign"/></option>
	<option value="2" <c:if test="${selectedHealthCenterType == 2}">selected</c:if>><spring:message code="PatientViewRestriction.healthcenter.last.encounter"/></option>
	<option value="3" <c:if test="${selectedHealthCenterType == 3}">selected</c:if>><spring:message code="PatientViewRestriction.healthcenter.ever.belong"/></option>									
</select>
<br/>
<a href="javascript:onSave();"><input type="button" value="<spring:message code="PatientViewRestriction.save"/>"/></a>
</div>
<%@ include file="/WEB-INF/template/footer.jsp" %>